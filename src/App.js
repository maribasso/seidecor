import './App.css';
import Api from './API'
import { useState, useEffect } from 'react';

function App() {
  const [categories_list, set_categories_list] = useState([]);
  const [course_list, set_course_list] = useState([]);

  useEffect(function(){
    loadData();
  },[])

  async function loadData(){
    const company_info = await Api.Company.get.info()
    const categories = await Api.Categories.get.all()
    set_categories_list(categories);
  }

  async function buscarCursos(categorie_id){
    const courses = await Api.Courses.get.byCategorie(categorie_id)
    set_course_list(courses);
  }
  
  return (
    <div className="App">
      <h3>Lista de Cursos</h3><br></br>
      {categories_list.map(function(categorie){
        return <p><button onClick={()=>{buscarCursos(categorie.id)}}> {categorie.name}</button></p>
      })}
      <h3>Dados dos Cursos</h3><br></br>
       {course_list.map(function(course){ 
        return <li>{course.name}</li>
      })}
    </div>
  );
}
export default App;
