import MOCK_COMPANY_INFO from '../MOCKS/company_info.json';
import MOCK_CATEGORIES from '../MOCKS/categories.json';
import MOCK_COURSES from '../MOCKS/courses.json';

const Company = {
    get: {
        info: async ()=>  {
            return new Promise((resolve,reject)=>{
                setTimeout(()=>{
                    resolve(MOCK_COMPANY_INFO)
                },2000) 
            })
        }
    },
    set: {

    }
}

const Categories = { 
    get:{
        all: async ()=>  {
            return new Promise((resolve,reject)=>{
                setTimeout(()=>{
                    resolve(MOCK_CATEGORIES)
                },2000) 
            })
        }
        
    },
    set:{

    }
}

const Courses = { 
    get:{
        byCategorie: async (categorie_id)=>  {
            return new Promise((resolve,reject)=>{
                setTimeout(()=>{
                    const courses = MOCK_COURSES.filter((courses)=>{ 
                        const categories = courses.categories.find((id)=>{
                            return id == categorie_id
                        })
                        console.log(courses.categories);
                        return categories; 
                    })
                    resolve(courses)
                },2000) 
            })
        }
        
    },
    set:{

    }
}

const API = {
    Company,
    Categories,
    Courses
}

export default API;